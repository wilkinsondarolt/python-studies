#encode=utf-8
import cherrypy
import log
import json

class main(object):    
    @cherrypy.expose
    def index(self):
        return """<html>
          <head></head>
          <body>
            <form method="get" action="consultaquantidade"><button type="submit">Consultar registros</button></form>
            <form method="get" action="insere"><button type="submit">Inserir registro</button></form>
          </body>
        </html>"""
    @cherrypy.expose
    def consultaquantidade(self):
        return str(log.consultar({}))
    
    @cherrypy.expose
    def consultarlogs(self):
        logs = json.loads(log.consultar({}))
        logview = ''
        for loginfo in logs:
            logview += '<tr><td>'+ loginfo['descricao'] + '</td><td>' + loginfo['usuario'] + '</td></tr>'            
        return "<html><head></head><body><table>" + logview + "</table></body></html>"        

    @cherrypy.expose    
    def consulta(self, *args, **kwargs):
        return log.consultar(kwargs)

    @cherrypy.expose
    def insere(self, *args, **kwargs):
        return log.inserir(kwargs)
 
if __name__ == '__main__':
    cherrypy.config.update('server.conf')
    cherrypy.quickstart(main(), '/', '')
