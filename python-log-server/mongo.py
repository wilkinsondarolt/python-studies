#encode=utf-8
from pymongo import MongoClient
from bson import json_util
import json

def mongocursortojson(cursor):
    return json_util.dumps(cursor, default=json_util.default)    

def getdb(databasename, collectionname):
    client = MongoClient()
    database = client.get_database(databasename)    
    return database.get_collection(collectionname)

def consulta(params):
    result = getdb('ema', 'log').find(params).limit(2500)
    return mongocursortojson(result)

def insere(registro):
    result = getdb('ema', 'log').insert_one(registro).inserted_id
    return mongocursortojson(result)
