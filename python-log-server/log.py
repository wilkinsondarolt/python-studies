#encode=utf-8
import mongo
import datetime

def inserir(params):
    return mongo.insere({"origem": params.get('origem', 0),
                         "idorigem": params.get('idorigem', 0),
                         "usuario": params.get('usuario', 'ANONIMO'),
                         "datahora": datetime.datetime.utcnow(),
                         "tipoconexao": params.get('tipoconexao', 13),
                         "descricao": params.get('descricao', '***SEM TEXTO***'),
                         "tipolog":  params.get('tipolog', 0),
                         "tipoorigem":  params.get('tipoorigem', 0),
                         "idorigemitem":  params.get('idorigemitem', 0),
                         "origemtexto": ''})

def consultar(params):
    fields = {}
    if params.has_key('origem'):
        fields['origem'] = params.get('origem')
    if params.has_key('idorigem'):
        fields['idorigem'] = params.get('idorigem')
    datahora = {}
    if params.has_key('dataini'):
        datahora['$gte'] = datetime.datetime.strptime(params.get('dataini') + ' 00:00:00', '%d/%m/%Y %H:%M:%S')        
    if params.has_key('datafim'):
        datahora['$lte'] = datetime.datetime.strptime(params.get('datafim') + ' 23:59:59', '%d/%m/%Y %H:%M:%S')
    if len(datahora):
        fields['datahora'] = datahora
    return mongo.consulta(fields)