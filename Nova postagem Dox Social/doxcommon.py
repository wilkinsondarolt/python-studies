import emarequests
import hashlib
import json

def login(user, password, navegador):
  return emarequests.post(emarequests.host_url() + 'acesso',
                          {'asUserName': user.upper(),
                           'asSenha': hashlib.md5(user + password).hexdigest(),
                           'asNavegador': navegador,
                           'asVersaoNavegador': ''},
                          '')

def retornadadosfk(hash, nomefk, filtro, valoratual):
    return emarequests.post(emarequests.host_url() + 'retornadadosfk',
                            {'aoJson': json.dumps({'NOMEFK': nomefk,
                                                   'FILTRO': filtro,
                                                   'VALORATUAL': valoratual})},
                             hash)

def dox_insere_blog(conteudo, alvo, blogcomment, visibilidade, anexos):
  return emarequests.post(emarequests.host_url() + 'insereblog',
                          json.dumps({'asTexto': conteudo,
                                      'asAlvo': alvo,
                                      'aiBlogComent': blogcomment,
                                      'aiVisibilidade': visibilidade,
                                      'anexos': anexos}))
