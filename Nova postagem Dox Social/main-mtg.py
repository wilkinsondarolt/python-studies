#-*- coding: ISO-8859-1 -*-
import shutil
import requests
import os

dburl = 'https://api.deckbrew.com/mtg/'

r = requests.get(dburl + 'cards',
                 {'name': 'Duress'}).json()
for cardinfo in r:
    cardfolder = 'images/'+ cardinfo['name'] + '/'
    if not os.path.isdir(cardfolder):
        os.mkdir(cardfolder)
    for edition in cardinfo['editions']:
        img = requests.get(edition['image_url'], stream=True)
        imgfile = cardfolder + edition['set_id'] + '.png'

        if not os.path.isfile(imgfile):
            out_file = open(imgfile, 'wb')
            shutil.copyfileobj(img.raw, out_file)
            print 'Concluido download da imagem do set -> ' + edition['set_id']
        del img
            
        




