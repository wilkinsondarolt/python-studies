import emarequests
import base64

def retorna_processo_anexo(idprocesso, idprocedimento, loginhash):
  return emarequests.post(emarequests.host_url() + 'retornaprocessoanexo',
                          {'aiProcesso': idprocesso,
                           'aiProcedimento': idprocedimento},
                          loginhash)

def download_processo_anexo(idprocesso, idanexo, loginhash):
  return emarequests.get(emarequests.host_url() + 'downloadprocessoanexo',
                          {'aiProcesso': base64.b64encode(str(idprocesso)),
                           'aiAnexo': base64.b64encode(str(idanexo))},
                          loginhash)

