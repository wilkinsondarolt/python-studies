#-*- coding: ISO-8859-1 -*-

import urllib2
import requests
import json
from requests.auth import HTTPProxyAuth

headers   = {'User-Agent' : "Magic Browser"}
proxy = urllib2.ProxyHandler({})
opener = urllib2.build_opener(proxy)
urllib2.install_opener(opener)

try:
  req = urllib2.Request('http://www.ligamagic.com.br/?view=cards%2Fsearch&card=Duress', headers=headers)
  resp = urllib2.urlopen(req).read()
  print(resp)
except urllib2.HTTPError, e:
  print e.fp.read()


