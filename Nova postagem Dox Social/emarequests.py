import requests

proxy_server_conf = '192.168.0.254:3128'
proxydict = {'http': proxy_server_conf,
             'https': proxy_server_conf,
             'ftp': proxy_server_conf}

def proxy_config():
  session = requests.Session()
  session.proxies = proxydict
  session.trust_env = False

def host_url():
  return 'http://localhost/api/'

def get(url, data, loginhash):
  return requests.get(url, data=data, proxies=proxydict, headers={'HASH': loginhash})

def post(url, data, loginhash):
  if not loginhash == '':
    return requests.post(url, data, headers={'HASH': loginhash})
  else:
    return requests.post(url, data)
