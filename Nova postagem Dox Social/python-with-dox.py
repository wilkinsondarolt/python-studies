#-*- coding: ISO-8859-1 -*-

import urllib
import urllib2
import requests
import hashlib
import json
import datetime
import base64
import io

# Configura��o de proxy
proxy_server_conf = 'http://firewall:8080'
proxydict = {'http': proxy_server_conf,
             'https': proxy_server_conf,
             'ftp': proxy_server_conf}

# Configura��o de usu�rio
# Dados para login
username = 'GERENTE'
password = username + '&m4pr0'

### method definition
def returnbase64file(path):
    arquivo = open(path, 'rb')
    return base64.b64encode(arquivo.read())    

#### URL ACCESS
def setupurlproxy():
    proxy = urllib2.ProxyHandler({})
    opener = urllib2.build_opener(proxy)
    urllib2.install_opener(opener)

def geturl(url, parameters):
    fullurl = url
    if not parameters == '':
        furllurl = fullurl + parameters
    return urllib2.urlopen(fullurl)

## REQUESTS
def request_get(url):
    return requests.get(url, proxies=proxydict)

def request_post(url, data, loginhash = ''):    
    if not loginhash == '':
        headers = {'HASH': loginhash}
        return requests.post(url, data, headers=headers)
    else:
        return requests.post(url, data)

def dox_login():
    return request_post('http://localhost/api/acesso',
                 {'asUserName': username.upper(),
                  'asSenha': hashlib.md5(password).hexdigest(),
                  'asNavegador': 'python',
                  'asVersaoNavegador': ''})

def dox_insere_blog(novo_post, loginhash):
    return request_post('http://localhost/api/insereblog',
                        {'aoJson': novo_post},
                        loginhash)

def main_app():
    r = dox_login()
    
    if r.status_code == 200:
        loginhash = json.loads(r.text)['HASH']
        data = datetime.datetime.now()
        texto = '%02d/%02d/%04d %02d:%02d:%02d' % (data.day,
                                               data.month,
                                               data.year,
                                               data.hour,
                                               data.minute,
                                               data.second) + ' - ' +  \
            'Nova postagem inserida via python.'
        alvo = ''
        # 8 - V�deo / 1 - Imagem / 3 - Link
        anexos = [{'IDTIPODOCUMENTO': 7,                   
                   'ORIGEM': 13,
                   'DESCRICAO':'https://www.youtube.com/watch?v=DCXfv2WvVwc',
                   'OBSERVACAO': 'https://www.youtube.com/watch?v=DCXfv2WvVwc',
                   'TIPO':8,
                   'NOMEORIGINAL': 'https://www.youtube.com/watch?v=DCXfv2WvVwc'},
                  {'IDTIPODOCUMENTO': 7,
                   'ORIGEM': 13,
                   'DESCRICAO': '01-bLxcjh3',
                   'EXTENSAO': '.png',
                   'OBSERVACAO': '',
                   'TIPO': 1,
                   'ANEXO': returnbase64file('01-bLxcjh3.png'),
                   'NOMEORIGINAL': '01-bLxcjh3'},
                  {'IDTIPODOCUMENTO': 7,                   
                   'ORIGEM': 13,
                   'DESCRICAO':'http://www.ligamagic.com.br',
                   'OBSERVACAO': 'http://www.ligamagic.com.br',
                   'TIPO':3,
                   'NOMEORIGINAL': 'http://www.ligamagic.com.br'}]
        novo_post = json.dumps({'asTexto' : texto,
                                'asAlvo' : alvo,
                                'aiBlogComent' : 0,
                                'aiVisibilidade': 0,
                                'anexos':anexos})
        
        r = dox_insere_blog(novo_post, loginhash)
        if r.status_code == 200:
            print 'Postado com sucesso'
        else:
            print 'Erro ao efetuar a postagem'
    else:
        print 'Erro ao efetuar login.'
    
