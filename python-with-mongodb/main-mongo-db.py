#encode=utf-8
from pymongo import MongoClient
from bson import json_util

client = MongoClient()
db = client.test

def busca(termos):
    cursor = db.restaurants.find(termos)
    with open('results.json', 'w') as result_file:
        result_file.write(json_util.dumps(cursor,default=json_util.default))
        
busca({"cuisine": "Irish"})
