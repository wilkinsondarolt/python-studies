#encode=utf-8
import requests
import pymongo
import os

dataset = 'primer-dataset.json'

# Verifica existencia do arquivo de database
if not os.path.isfile(dataset):
    r = requests.get("https://raw.githubusercontent.com/mongodb/docs-assets/primer-dataset/primer-dataset.json")
    if r.status_code == 200:
        with open(dataset, 'w') as dataset_file:
            dataset_file.write(r.text) 
