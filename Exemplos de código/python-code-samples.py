#-*- coding: ISO-8859-1 -*-

import string
import UserString

# Defini��o de um m�todo
def printlist(alist=None):
    for cod, instrumento in enumerate(alist):
        print cod+1, '->', instrumento

# Cria��o de uma lista
instrumentos = ['Baixo', 'Bateria', 'Guitarra']
print instrumentos

# Lista est�stica (tupla) "append" e "pop" resultar�o em erro
cards = ('Duress', 'Counterspell', 'Remand')

# Retirar elemento da lista
instrumentos. pop(0)

# Adicionar elemento a lista
instrumentos.append('Baixo')
instrumentos.append('Banjo')
print instrumentos

# Ordernar os elementos da lista
instrumentos.sort()
print instrumentos

# Imprimir enumerado
printlist(instrumentos)

# Exemplo de IFSTR
variavel = 1 if 1 != 1 else 2
print variavel

# Comando para "Format"
print('Agora s�o %02d:%02d.' % (18, 05))

# Inverte o texto
print('Wilkinson'[::-1])

# Exemplos de c�digo com quebra de linha
a = 7 * 3 + \
    5 / 2
b = ['a', 'b', 'c',
     'd', 'e']

# Monta uma lista do valor inicial at� o final -1
c = range(1, 11)
print a, b, c

# Exemplos de FOR com lista
for i in [234, 654, 378, 798]:
    if i % 3 == 0:
        print i, ' / 3 =', i/3

# Exemplos de isner��o de valores
temp = int(raw_input('Informe a temperatura: '))
ar = raw_input('Ar est� ligado?')

# Efetuar manipula��o em vari�veis em somente uma linha 
temp = temp-3 if ar.upper() == 'S' else temp

# Exemplo de constru��o de uma cadeia de ifs
if temp < 0:
    print 'Congelando'
elif 0 <= temp <= 20:
    print 'Frio'
elif 21 <= temp <= 25:
    print 'Normal'
elif 26 <= temp <= 35:
    print 'Quente'
else:
    print 'Muito quente!'
    
# Condicionais de uma linha podem ser escritas dessa forma
if ar.upper() == 'S': print 'Melhor assim'

# Templates
template = string.Template('$aviso aconteceu em $quando')
aviso = template.substitute({'aviso': 'Falta de eleticidade',
                             'quando': '03 de abril de 2002'})
print aviso

# Strings mut�veis
mutavel = UserString.MutableString('Python')
print mutavel
mutavel[0] = 'p'
print mutavel
