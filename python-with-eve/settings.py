import schemas
MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DBNAME = 'ema'
DATE_FORMAT = '%d/%m/%Y %H:%M:%S'
PAGINATION_DEFAULT = 50
DOMAIN = {'log': schemas.log_definition}