## SCHEMA DEFINITION FOR LOG
log_schema = {'origem': {'type': 'integer'},
              'idorigem': {'type': 'integer'},
              'tipoconexao': {'type': 'integer'},
              'usuario': {'type': 'string', 'minlength': 1, 'maxlength': 80},
              'descricao': {'type': 'string', 'minlength': 1, 'required': True},
              'tipolog': {'type': 'integer'},
              'tipoorigem': {'type': 'integer'},
              'idorigemitem' : {'type': 'integer'}, 
              'origemtexto': {'type': 'string'},
              'datahora': {'type': 'datetime'}}

log_definition = {'item_title': 'log',
                  'cache_control': 'max-age=10,must-revalidate',
                  'cache_expires': 10,
                  'resource_methods': ['GET', 'POST'],
                  'schema': log_schema}              